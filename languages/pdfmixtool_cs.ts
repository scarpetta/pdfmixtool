<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../src/aboutdialog.cpp" line="33"/>
        <source>About PDF Mix Tool</source>
        <translation>O aplikaci PDF Mix Tool</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="41"/>
        <source>Close</source>
        <translation>Zavřít</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="51"/>
        <source>Version %1</source>
        <translation>Verze %1</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="67"/>
        <source>Website</source>
        <translation>Web stránky</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="74"/>
        <source>About</source>
        <translation>O aplikaci</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="64"/>
        <source>An application to perform common editing operations on PDF files.</source>
        <translation>Aplikace pro provádění nejběžnějších editačních operací se soubory PDF.</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="89"/>
        <source>Translators</source>
        <translation>Překladatelé</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="98"/>
        <source>Credits</source>
        <translation>Zásluhy</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="122"/>
        <source>License</source>
        <translation>Licence</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="135"/>
        <source>Submit a pull request</source>
        <translation>Poslat návrh na změnu (pull request)</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="136"/>
        <source>Report a bug</source>
        <translation>Nahlásit chybu</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="137"/>
        <source>Help translating</source>
        <translation>Pomoci s překladem</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="144"/>
        <source>Contribute</source>
        <translation>Přispějte</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="160"/>
        <source>Changelog</source>
        <translation>Seznam změn</translation>
    </message>
</context>
<context>
    <name>AbstractOperation</name>
    <message>
        <location filename="../src/operations/abstract_operation.cpp" line="41"/>
        <source>Save</source>
        <translation>Uložit</translation>
    </message>
    <message>
        <location filename="../src/operations/abstract_operation.cpp" line="102"/>
        <source>Overwrite File?</source>
        <translation>Přepsat soubor?</translation>
    </message>
    <message>
        <location filename="../src/operations/abstract_operation.cpp" line="103"/>
        <source>A file called «%1» already exists. Do you want to overwrite it?</source>
        <translation>Soubor s názvem «%1» již existuje. Chcete jej přepsat?</translation>
    </message>
    <message>
        <location filename="../src/operations/abstract_operation.cpp" line="109"/>
        <source>Always overwrite</source>
        <translation>Vždy přepsat</translation>
    </message>
    <message>
        <location filename="../src/operations/abstract_operation.cpp" line="127"/>
        <source>Save PDF file</source>
        <translation>Uložit PDF soubor</translation>
    </message>
    <message>
        <location filename="../src/operations/abstract_operation.cpp" line="131"/>
        <source>PDF files</source>
        <translation>PDF soubory</translation>
    </message>
</context>
<context>
    <name>AddEmptyPages</name>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="32"/>
        <source>Add empty pages</source>
        <translation>Přidat prázdné stránky</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="45"/>
        <source>Count:</source>
        <translation>Počet:</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="52"/>
        <source>Page size</source>
        <translation>Velikost stránky</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="55"/>
        <source>Same as document</source>
        <translation>Stejně jako dokument</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="59"/>
        <source>Custom:</source>
        <translation>Přizpůsobit:</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="85"/>
        <source>Standard:</source>
        <translation>Standardní:</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="95"/>
        <source>Portrait</source>
        <translation>Na výšku</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="97"/>
        <source>Landscape</source>
        <translation>Na šířku</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="100"/>
        <source>Location</source>
        <translation>Umístění</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="103"/>
        <source>Before</source>
        <translation>Předtím</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="106"/>
        <source>After</source>
        <translation>Potom</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="110"/>
        <source>Page:</source>
        <translation>Strana:</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="124"/>
        <source>Save as…</source>
        <translation>Uložit jako…</translation>
    </message>
</context>
<context>
    <name>AlternateMix</name>
    <message>
        <location filename="../src/operations/alternate_mix.cpp" line="26"/>
        <source>Alternate mix</source>
        <translation type="unfinished">Alternativní mix</translation>
    </message>
</context>
<context>
    <name>Booklet</name>
    <message>
        <location filename="../src/operations/booklet.cpp" line="31"/>
        <source>Booklet</source>
        <translation>Brožura</translation>
    </message>
    <message>
        <location filename="../src/operations/booklet.cpp" line="41"/>
        <source>Left</source>
        <translation>Vlevo</translation>
    </message>
    <message>
        <location filename="../src/operations/booklet.cpp" line="42"/>
        <source>Right</source>
        <translation>Vpravo</translation>
    </message>
    <message>
        <location filename="../src/operations/booklet.cpp" line="43"/>
        <source>Binding:</source>
        <translation>Vazba:</translation>
    </message>
    <message>
        <location filename="../src/operations/booklet.cpp" line="44"/>
        <source>Use last page as back cover:</source>
        <translation>Použít poslední stranu jako zadní obálku:</translation>
    </message>
    <message>
        <location filename="../src/operations/booklet.cpp" line="46"/>
        <source>Generate two booklets in one sheet:</source>
        <translation>Generování dvou brožur na jednom listu:</translation>
    </message>
    <message>
        <location filename="../src/operations/booklet.cpp" line="54"/>
        <source>Generate booklet</source>
        <translation>Vytvořit brožuru</translation>
    </message>
    <message>
        <location filename="../src/operations/booklet.cpp" line="75"/>
        <source>Save booklet PDF file</source>
        <translation>Uložit brožuru ve formátu PDF</translation>
    </message>
    <message>
        <location filename="../src/operations/booklet.cpp" line="79"/>
        <source>PDF files</source>
        <translation>PDF soubory</translation>
    </message>
</context>
<context>
    <name>DeletePages</name>
    <message>
        <location filename="../src/operations/delete_pages.cpp" line="32"/>
        <source>Delete pages</source>
        <translation>Smazat stránky</translation>
    </message>
    <message>
        <location filename="../src/operations/delete_pages.cpp" line="56"/>
        <source>Save as…</source>
        <translation>Uložit jako…</translation>
    </message>
</context>
<context>
    <name>EditDocumentInfo</name>
    <message>
        <location filename="../src/operations/edit_document_info.cpp" line="14"/>
        <source>Document information</source>
        <translation>Informace o dokumentu</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_document_info.cpp" line="24"/>
        <source>Title:</source>
        <translation>Název:</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_document_info.cpp" line="27"/>
        <source>Author:</source>
        <translation>Autor:</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_document_info.cpp" line="30"/>
        <source>Subject:</source>
        <translation>Předmět:</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_document_info.cpp" line="33"/>
        <source>Keywords:</source>
        <translation>Klíčová slova:</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_document_info.cpp" line="36"/>
        <source>Creator:</source>
        <translation>Vytvořil:</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_document_info.cpp" line="39"/>
        <source>Producer:</source>
        <translation>Vyrobil:</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_document_info.cpp" line="42"/>
        <source>Creation date:</source>
        <translation>Čas vytvoření:</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_document_info.cpp" line="46"/>
        <source>Modification date:</source>
        <translation>Změněno dne:</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_document_info.cpp" line="75"/>
        <source>Save as…</source>
        <translation>Uložit jako…</translation>
    </message>
</context>
<context>
    <name>EditMultipageProfileDialog</name>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="29"/>
        <source>Edit multipage profile</source>
        <translation>Upravit vícestránkový profil</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="37"/>
        <source>Name:</source>
        <translation>Jméno:</translation>
    </message>
</context>
<context>
    <name>EditPageLayout</name>
    <message>
        <location filename="../src/operations/edit_page_layout.cpp" line="34"/>
        <source>Pages layout</source>
        <translation>Rozložení stránek</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_page_layout.cpp" line="44"/>
        <source>Apply to:</source>
        <translation>Použít na:</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_page_layout.cpp" line="70"/>
        <source>Save as…</source>
        <translation>Uložit jako…</translation>
    </message>
</context>
<context>
    <name>EditPdfEntryDialog</name>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="37"/>
        <source>Edit PDF files&apos; properties</source>
        <translation>Upravit vlastnosti PDF souboru</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="40"/>
        <source>No rotation</source>
        <translation>Neotáčet</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="45"/>
        <source>Disabled</source>
        <translation>Neaktivní</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="90"/>
        <source>OK</source>
        <translation>Budiž</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="96"/>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="103"/>
        <source>Multipage:</source>
        <translation>Více stránek:</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="105"/>
        <source>Rotation:</source>
        <translation>Otáčení:</translation>
    </message>
</context>
<context>
    <name>ExtractPages</name>
    <message>
        <location filename="../src/operations/extract_pages.cpp" line="55"/>
        <source>Output PDF base name:</source>
        <translation>Název výstupního PDF:</translation>
    </message>
    <message>
        <location filename="../src/operations/extract_pages.cpp" line="51"/>
        <source>Extract to individual PDF files</source>
        <translation>Rozbalení do jednotlivých souborů PDF</translation>
    </message>
    <message>
        <location filename="../src/operations/extract_pages.cpp" line="35"/>
        <source>Extract pages</source>
        <translation>Rozbalit stránky</translation>
    </message>
    <message>
        <location filename="../src/operations/extract_pages.cpp" line="64"/>
        <location filename="../src/operations/extract_pages.cpp" line="79"/>
        <source>Extract…</source>
        <translation>Rozbalit…</translation>
    </message>
    <message>
        <location filename="../src/operations/extract_pages.cpp" line="74"/>
        <location filename="../src/operations/extract_pages.cpp" line="174"/>
        <source>Extract to single PDF</source>
        <translation>Rozbalit do jednoho PDF</translation>
    </message>
    <message>
        <location filename="../src/operations/extract_pages.cpp" line="116"/>
        <source>Select save directory</source>
        <translation>Vyberte adresář pro uložení</translation>
    </message>
    <message>
        <location filename="../src/operations/extract_pages.cpp" line="178"/>
        <source>PDF files</source>
        <translation>PDF soubory</translation>
    </message>
</context>
<context>
    <name>InputPdfFileDelegate</name>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="109"/>
        <source>All</source>
        <translation>Vše</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="81"/>
        <source>Page order:</source>
        <translation>Pořadí stránek:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="84"/>
        <source>reverse</source>
        <translation>obráceně</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="86"/>
        <source>forward</source>
        <translation>dopředu</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="122"/>
        <source>Pages:</source>
        <translation>Strany:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="125"/>
        <source>Multipage:</source>
        <translation>Více stránek:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="130"/>
        <source>Disabled</source>
        <translation>Neaktivní</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="133"/>
        <source>Rotation:</source>
        <translation>Otočení:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="136"/>
        <source>Outline entry:</source>
        <translation>Zadání osnovy:</translation>
    </message>
</context>
<context>
    <name>InputPdfFileWidget</name>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="60"/>
        <source>Reverse page order:</source>
        <translation>Obrácené pořadí stránek:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="74"/>
        <source>Disabled</source>
        <translation>Neaktivní</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="82"/>
        <source>New custom profile…</source>
        <translation>Nový vlastní profil…</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="84"/>
        <source>No rotation</source>
        <translation>Neotáčet</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="89"/>
        <source>Pages:</source>
        <translation>Stránky:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="91"/>
        <source>Multipage:</source>
        <translation>Více stránek:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="93"/>
        <source>Rotation:</source>
        <translation>Otočení:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="95"/>
        <source>Outline entry:</source>
        <translation>Osnova:</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="98"/>
        <source>About</source>
        <translation>O aplikaci</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="88"/>
        <source>Menu</source>
        <translation>Nabídka</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="93"/>
        <source>Multipage profiles…</source>
        <translation>Vícestránkové profily…</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="103"/>
        <source>Exit</source>
        <translation>Konec</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="178"/>
        <source>Open PDF file…</source>
        <translation>Otevřít PDF soubor…</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="292"/>
        <source>PDF files</source>
        <translation>PDF soubory</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="307"/>
        <source>Error opening file</source>
        <translation>Chyba při otevírání souboru</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="332"/>
        <source>Output pages: %1</source>
        <translation>Stránky na výstupu: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="290"/>
        <source>Select a PDF file</source>
        <translation>Vybrat PDF soubor</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="362"/>
        <source>Files saved in %1.</source>
        <translation>Soubory uloženy v %1.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="368"/>
        <source>File %1 saved.</source>
        <translation>Soubor %1 uložen.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="378"/>
        <source>Error generating the PDF</source>
        <translation>Chyba při generování PDF</translation>
    </message>
</context>
<context>
    <name>Merge</name>
    <message>
        <location filename="../src/operations/merge.cpp" line="42"/>
        <source>Merge PDF files</source>
        <translation>Sloučení souborů PDF</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="70"/>
        <source>Edit</source>
        <translation>Upravit</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="71"/>
        <source>View</source>
        <translation>Zobrazit</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="74"/>
        <source>Main toolbar</source>
        <translation>Hlavní lišta</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="81"/>
        <source>Add PDF file</source>
        <translation>Přidat PDF soubor</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="86"/>
        <source>Select one or more PDF files to open</source>
        <translation>Vyberte jeden či více PDF souborů k otevření</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="88"/>
        <location filename="../src/operations/merge.cpp" line="589"/>
        <source>PDF files</source>
        <translation>PDF soubory</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="93"/>
        <source>Move up</source>
        <translation>Posunout nahoru</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="98"/>
        <source>Move down</source>
        <translation>Posunout dolů</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="103"/>
        <source>Remove file</source>
        <translation>Odstranit soubor</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="113"/>
        <source>Load files list</source>
        <translation>Načíst seznam souborů</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="118"/>
        <source>Save files list</source>
        <translation>Uložit seznam souborů</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="152"/>
        <location filename="../src/operations/merge.cpp" line="156"/>
        <source>Generate PDF</source>
        <translation>Vytvořit PDF</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="332"/>
        <source>Select the JSON file containing the files list</source>
        <translation>Vyberte soubor JSON obsahující seznam souborů</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="334"/>
        <location filename="../src/operations/merge.cpp" line="352"/>
        <source>JSON files (*.json)</source>
        <translation>soubory JSON (*.json)</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="340"/>
        <source>Error while reading the JSON file!</source>
        <translation>Chyba při načítání souboru JSON!</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="341"/>
        <source>An error occurred while reading the JSON file!</source>
        <translation>Při načítání souboru JSON došlo k chybě!</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="348"/>
        <source>Select a JSON file</source>
        <translation>Vybrat JSON soubor</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="427"/>
        <source>Error opening file</source>
        <translation>Chyba při otevírání souboru</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="561"/>
        <source>&lt;p&gt;Output pages of file &lt;b&gt;%1&lt;/b&gt; are badly formatted. Please make sure you complied with the following rules:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;intervals of pages must be written indicating the first page and the last page separated by a dash (e.g. &quot;1-5&quot;);&lt;/li&gt;&lt;li&gt;single pages and intervals of pages must be separated by spaces, commas or both (e.g. &quot;1, 2, 3, 5-10&quot; or &quot;1 2 3 5-10&quot;);&lt;/li&gt;&lt;li&gt;all pages and intervals of pages must be between 1 and the number of pages of the PDF file;&lt;/li&gt;&lt;li&gt;only numbers, spaces, commas and dashes can be used. All other characters are not allowed.&lt;/li&gt;&lt;/ul&gt;</source>
        <translation>&lt;p&gt;Stránky výstupního souboru &lt;b&gt;%1&lt;/b&gt; jsou chybně zadané. Ujistěte se prosím, že jste dodrželi následující pravidla:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;intervaly stránek musí být zadány číslem první a poslední stránky, oddělenými pomlčkou (např. &quot;1-5&quot;);&lt;/li&gt;&lt;li&gt;jednotlivá čísla stránek musí být navzájem oddělena čárkou, mezerou nebo obojím (např. &quot;1, 2, 3, 5-10&quot; nebo &quot;1 2 3 5-10&quot;);&lt;/li&gt;&lt;li&gt;všechny stránky a intervaly stránek musí být větší nebo rovny číslu 1 a menší nebo rovny celkovému počtu strany PDF dokumentu;&lt;/li&gt;&lt;li&gt;lze použít pouze čísla, mezery, čárky a pomlčky. Jiné znaky nejsou přípustné.&lt;/li&gt;&lt;/ul&gt;</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="578"/>
        <source>PDF generation error</source>
        <translation>Chyba při vytváření PDF</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="585"/>
        <source>Save PDF file</source>
        <translation>Uložit PDF soubor</translation>
    </message>
</context>
<context>
    <name>MultipageEditor</name>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="34"/>
        <source>Standard size</source>
        <translation>Standardní rozměr</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="35"/>
        <source>Custom size</source>
        <translation>Vlastní rozměr</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="44"/>
        <source>Portrait</source>
        <translation>Na výšku</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="45"/>
        <source>Landscape</source>
        <translation>Na šířku</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="65"/>
        <source>Right-to-left</source>
        <translation>Zprava doleva</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="67"/>
        <location filename="../src/widgets/multipage_editor.cpp" line="202"/>
        <source>Left</source>
        <translation>Vlevo</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="68"/>
        <location filename="../src/widgets/multipage_editor.cpp" line="72"/>
        <source>Center</source>
        <translation>Na střed</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="69"/>
        <location filename="../src/widgets/multipage_editor.cpp" line="205"/>
        <source>Right</source>
        <translation>Vpravo</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="71"/>
        <location filename="../src/widgets/multipage_editor.cpp" line="208"/>
        <source>Top</source>
        <translation>Nahoře</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="73"/>
        <location filename="../src/widgets/multipage_editor.cpp" line="211"/>
        <source>Bottom</source>
        <translation>Dole</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="106"/>
        <source>Page size</source>
        <translation>Velikost stránky</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="125"/>
        <source>Orientation:</source>
        <translation>Orientace:</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="132"/>
        <source>Width:</source>
        <translation>Šířka:</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="135"/>
        <source>Height:</source>
        <translation>Výška:</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="162"/>
        <source>Pages layout</source>
        <translation>Rozložení stránek</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="169"/>
        <source>Rows:</source>
        <translation>Řádky:</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="172"/>
        <source>Columns:</source>
        <translation>Sloupce:</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="175"/>
        <source>Spacing:</source>
        <translation>Odsazení:</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="181"/>
        <source>Pages alignment</source>
        <translation>Zarovnání stránek</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="188"/>
        <source>Horizontal:</source>
        <translation>Horizontálně:</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="191"/>
        <source>Vertical:</source>
        <translation>Vertikálně:</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="195"/>
        <source>Margins</source>
        <translation>Okraje</translation>
    </message>
</context>
<context>
    <name>MultipageProfilesManager</name>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="31"/>
        <source>New profile…</source>
        <translation>Nový profil…</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="35"/>
        <source>Delete profile</source>
        <translation>Smazat profil</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="40"/>
        <source>Manage multipage profiles</source>
        <translation>Spravovat vícestránkové profily</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="72"/>
        <source>Edit profile</source>
        <translation>Upravit profil</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="118"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="120"/>
        <source>Custom profile</source>
        <translation>Vlastní profil</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="179"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="188"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="202"/>
        <source>Error</source>
        <translation>Chyba</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="180"/>
        <source>Profile name can not be empty.</source>
        <translation>Název profilu musí být vyplněn.</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="185"/>
        <source>Disabled</source>
        <translation>Neaktivní</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="189"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="203"/>
        <source>Profile name already exists.</source>
        <translation>Název profilu již existuje.</translation>
    </message>
</context>
<context>
    <name>PagesSelector</name>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="46"/>
        <source>Pages:</source>
        <translation>Stránky:</translation>
    </message>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="47"/>
        <source>Even pages</source>
        <translation>Sudé strany</translation>
    </message>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="48"/>
        <source>Odd pages</source>
        <translation>Liché strany</translation>
    </message>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="50"/>
        <source>All pages</source>
        <translation>Všechny strany</translation>
    </message>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="125"/>
        <source>&lt;p&gt;Page intervals are badly formatted. Please make sure you complied with the following rules:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;intervals of pages must be written indicating the first page and the last page separated by a dash (e.g. &quot;1-5&quot;);&lt;/li&gt;&lt;li&gt;single pages and intervals of pages must be separated by spaces, commas or both (e.g. &quot;1, 2, 3, 5-10&quot; or &quot;1 2 3 5-10&quot;);&lt;/li&gt;&lt;li&gt;all pages and intervals of pages must be between 1 and the number of pages of the PDF file;&lt;/li&gt;&lt;li&gt;only numbers, spaces, commas and dashes can be used. All other characters are not allowed.&lt;/li&gt;&lt;/ul&gt;</source>
        <translation>&lt;p&gt;Rozsah stránek je chybně zadán. Ujistěte se prosím, že jste dodrželi následující pravidla:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;rozsahy stránek musí být zadány číslem první a poslední stránky, oddělenými pomlčkou (např. &quot;1-5&quot;);&lt;/li&gt;&lt;li&gt;jednotlivá čísla stránek musí být navzájem oddělena čárkou, mezerou nebo obojím (např. &quot;1, 2, 3, 5-10&quot; nebo &quot;1 2 3 5-10&quot;);&lt;/li&gt;&lt;li&gt;všechny stránky a intervaly stránek musí být větší nebo rovny číslu 1 a menší nebo rovny celkovému počtu strany PDF dokumentu;&lt;/li&gt;&lt;li&gt;lze použít pouze čísla, mezery, čárky a pomlčky. Jiné znaky nejsou přípustné.&lt;/li&gt;&lt;/ul&gt;</translation>
    </message>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="141"/>
        <source>Error</source>
        <translation>Chyba</translation>
    </message>
</context>
<context>
    <name>PdfInfoLabel</name>
    <message>
        <location filename="../src/widgets/pdfinfolabel.cpp" line="52"/>
        <source>portrait</source>
        <translation>na výšku</translation>
    </message>
    <message>
        <location filename="../src/widgets/pdfinfolabel.cpp" line="53"/>
        <source>landscape</source>
        <translation>na šířku</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/widgets/pdfinfolabel.cpp" line="63"/>
        <source>%n page(s)</source>
        <translation>
            <numerusform>%n strana</numerusform>
            <numerusform>%n strany</numerusform>
            <numerusform>%n stran</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>Rotate</name>
    <message>
        <location filename="../src/operations/rotate.cpp" line="32"/>
        <source>Rotate</source>
        <translation>Otočit</translation>
    </message>
    <message>
        <location filename="../src/operations/rotate.cpp" line="73"/>
        <source>Save as…</source>
        <translation>Uložit jako…</translation>
    </message>
</context>
</TS>
