<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ta">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../src/aboutdialog.cpp" line="33"/>
        <source>About PDF Mix Tool</source>
        <translation>PDF கலவை கருவி பற்றி</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="41"/>
        <source>Close</source>
        <translation>மூடு</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="51"/>
        <source>Version %1</source>
        <translation>பதிப்பு %1</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="64"/>
        <source>An application to perform common editing operations on PDF files.</source>
        <translation>PDF கோப்புகளில் பொதுவான திருத்துதல் செயல்பாடுகளைச் செய்வதற்கான பயன்பாடு.</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="67"/>
        <source>Website</source>
        <translation>வலைத்தளம்</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="74"/>
        <source>About</source>
        <translation>பற்றி</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="89"/>
        <source>Translators</source>
        <translation>மொழிபெயர்ப்பாளர்கள்</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="98"/>
        <source>Credits</source>
        <translation>வரவு</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="122"/>
        <source>License</source>
        <translation>உரிமம்</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="135"/>
        <source>Submit a pull request</source>
        <translation>இழுக்கும் கோரிக்கையை சமர்ப்பிக்கவும்</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="136"/>
        <source>Report a bug</source>
        <translation>ஒரு பிழையைப் புகாரளிக்கவும்</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="137"/>
        <source>Help translating</source>
        <translation>மொழிபெயர்க்க உதவுங்கள்</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="144"/>
        <source>Contribute</source>
        <translation>பங்களிப்பு</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="160"/>
        <source>Changelog</source>
        <translation>மாற்றபதிவு</translation>
    </message>
</context>
<context>
    <name>AbstractOperation</name>
    <message>
        <location filename="../src/operations/abstract_operation.cpp" line="41"/>
        <source>Save</source>
        <translation>சேமி</translation>
    </message>
    <message>
        <location filename="../src/operations/abstract_operation.cpp" line="102"/>
        <source>Overwrite File?</source>
        <translation>கோப்பை மேலெழுதவா?</translation>
    </message>
    <message>
        <location filename="../src/operations/abstract_operation.cpp" line="103"/>
        <source>A file called «%1» already exists. Do you want to overwrite it?</source>
        <translation>«%1» என்று அழைக்கப்படும் ஒரு கோப்பு ஏற்கனவே உள்ளது. அதை மேலெழுத விரும்புகிறீர்களா?</translation>
    </message>
    <message>
        <location filename="../src/operations/abstract_operation.cpp" line="109"/>
        <source>Always overwrite</source>
        <translation>எப்போதும் மேலெழுதும்</translation>
    </message>
    <message>
        <location filename="../src/operations/abstract_operation.cpp" line="127"/>
        <source>Save PDF file</source>
        <translation>PDF கோப்பைச் சேமிக்கவும்</translation>
    </message>
    <message>
        <location filename="../src/operations/abstract_operation.cpp" line="131"/>
        <source>PDF files</source>
        <translation>PDF கோப்புகள்</translation>
    </message>
</context>
<context>
    <name>AddEmptyPages</name>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="32"/>
        <source>Add empty pages</source>
        <translation>வெற்று பக்கங்களைச் சேர்க்கவும்</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="45"/>
        <source>Count:</source>
        <translation>எண்ணிக்கை:</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="52"/>
        <source>Page size</source>
        <translation>பக்க அளவு</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="55"/>
        <source>Same as document</source>
        <translation>ஆவணம் அதே</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="59"/>
        <source>Custom:</source>
        <translation>வழக்கம்:</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="85"/>
        <source>Standard:</source>
        <translation>தரநிலை:</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="95"/>
        <source>Portrait</source>
        <translation>உருவப்படம்</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="97"/>
        <source>Landscape</source>
        <translation>நிலப்பரப்பு</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="100"/>
        <source>Location</source>
        <translation>இடம்</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="103"/>
        <source>Before</source>
        <translation>முன்</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="106"/>
        <source>After</source>
        <translation>பிறகு</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="110"/>
        <source>Page:</source>
        <translation>பக்கம்:</translation>
    </message>
    <message>
        <location filename="../src/operations/add_empty_pages.cpp" line="124"/>
        <source>Save as…</source>
        <translation>சேமிக்கவும்…</translation>
    </message>
</context>
<context>
    <name>AlternateMix</name>
    <message>
        <location filename="../src/operations/alternate_mix.cpp" line="26"/>
        <source>Alternate mix</source>
        <translation>மாற்று கலவை</translation>
    </message>
</context>
<context>
    <name>Booklet</name>
    <message>
        <location filename="../src/operations/booklet.cpp" line="31"/>
        <source>Booklet</source>
        <translation>கையேடு</translation>
    </message>
    <message>
        <location filename="../src/operations/booklet.cpp" line="41"/>
        <source>Left</source>
        <translation>இடது</translation>
    </message>
    <message>
        <location filename="../src/operations/booklet.cpp" line="42"/>
        <source>Right</source>
        <translation>வலது</translation>
    </message>
    <message>
        <location filename="../src/operations/booklet.cpp" line="43"/>
        <source>Binding:</source>
        <translation>பிணைப்பு:</translation>
    </message>
    <message>
        <location filename="../src/operations/booklet.cpp" line="44"/>
        <source>Use last page as back cover:</source>
        <translation>கடைசி பக்கத்தை பின் அட்டையாகப் பயன்படுத்தவும்:</translation>
    </message>
    <message>
        <location filename="../src/operations/booklet.cpp" line="46"/>
        <source>Generate two booklets in one sheet:</source>
        <translation>ஒரு தாளில் இரண்டு கையேடுகளை உருவாக்குங்கள்:</translation>
    </message>
    <message>
        <location filename="../src/operations/booklet.cpp" line="54"/>
        <source>Generate booklet</source>
        <translation>கையேட்டை உருவாக்குங்கள்</translation>
    </message>
    <message>
        <location filename="../src/operations/booklet.cpp" line="75"/>
        <source>Save booklet PDF file</source>
        <translation>கையேடு PDF கோப்பை சேமிக்கவும்</translation>
    </message>
    <message>
        <location filename="../src/operations/booklet.cpp" line="79"/>
        <source>PDF files</source>
        <translation>PDF கோப்புகள்</translation>
    </message>
</context>
<context>
    <name>DeletePages</name>
    <message>
        <location filename="../src/operations/delete_pages.cpp" line="32"/>
        <source>Delete pages</source>
        <translation>பக்கங்களை நீக்கு</translation>
    </message>
    <message>
        <location filename="../src/operations/delete_pages.cpp" line="56"/>
        <source>Save as…</source>
        <translation>சேமிக்கவும்…</translation>
    </message>
</context>
<context>
    <name>EditDocumentInfo</name>
    <message>
        <location filename="../src/operations/edit_document_info.cpp" line="14"/>
        <source>Document information</source>
        <translation>ஆவணத் செய்தி</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_document_info.cpp" line="24"/>
        <source>Title:</source>
        <translation>தலைப்பு:</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_document_info.cpp" line="27"/>
        <source>Author:</source>
        <translation>ஆசிரியர்:</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_document_info.cpp" line="30"/>
        <source>Subject:</source>
        <translation>பொருள்:</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_document_info.cpp" line="33"/>
        <source>Keywords:</source>
        <translation>முக்கிய வார்த்தைகள்:</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_document_info.cpp" line="36"/>
        <source>Creator:</source>
        <translation>உருவாக்கியவர்:</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_document_info.cpp" line="39"/>
        <source>Producer:</source>
        <translation>தயாரிப்பாளர்:</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_document_info.cpp" line="42"/>
        <source>Creation date:</source>
        <translation>உருவாக்கும் தேதி:</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_document_info.cpp" line="46"/>
        <source>Modification date:</source>
        <translation>மாற்றும் தேதி:</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_document_info.cpp" line="75"/>
        <source>Save as…</source>
        <translation>சேமிக்கவும்…</translation>
    </message>
</context>
<context>
    <name>EditMultipageProfileDialog</name>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="29"/>
        <source>Edit multipage profile</source>
        <translation>மல்டிபேச் சுயவிவரத்தைத் திருத்தவும்</translation>
    </message>
    <message>
        <location filename="../src/editmultipageprofiledialog.cpp" line="37"/>
        <source>Name:</source>
        <translation>பெயர்:</translation>
    </message>
</context>
<context>
    <name>EditPageLayout</name>
    <message>
        <location filename="../src/operations/edit_page_layout.cpp" line="34"/>
        <source>Pages layout</source>
        <translation>பக்கங்கள் தளவமைப்பு</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_page_layout.cpp" line="44"/>
        <source>Apply to:</source>
        <translation>இதற்கு விண்ணப்பிக்கவும்:</translation>
    </message>
    <message>
        <location filename="../src/operations/edit_page_layout.cpp" line="70"/>
        <source>Save as…</source>
        <translation>சேமிக்கவும்…</translation>
    </message>
</context>
<context>
    <name>EditPdfEntryDialog</name>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="37"/>
        <source>Edit PDF files&apos; properties</source>
        <translation>PDF கோப்புகளின் பண்புகளைத் திருத்தவும்</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="40"/>
        <source>No rotation</source>
        <translation>சுழற்சி இல்லை</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="45"/>
        <source>Disabled</source>
        <translation>முடக்கப்பட்டது</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="90"/>
        <source>OK</source>
        <translation>சரி</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="96"/>
        <source>Cancel</source>
        <translation>ரத்துசெய்</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="103"/>
        <source>Multipage:</source>
        <translation>மல்டிபேச்:</translation>
    </message>
    <message>
        <location filename="../src/editpdfentrydialog.cpp" line="105"/>
        <source>Rotation:</source>
        <translation>சுழற்சி:</translation>
    </message>
</context>
<context>
    <name>ExtractPages</name>
    <message>
        <location filename="../src/operations/extract_pages.cpp" line="35"/>
        <source>Extract pages</source>
        <translation>பக்கங்களை பிரித்தெடுக்கவும்</translation>
    </message>
    <message>
        <location filename="../src/operations/extract_pages.cpp" line="51"/>
        <source>Extract to individual PDF files</source>
        <translation>தனிப்பட்ட PDF கோப்புகளுக்கு பிரித்தெடுக்கவும்</translation>
    </message>
    <message>
        <location filename="../src/operations/extract_pages.cpp" line="55"/>
        <source>Output PDF base name:</source>
        <translation>வெளியீட்டு PDF அடிப்படை பெயர்:</translation>
    </message>
    <message>
        <location filename="../src/operations/extract_pages.cpp" line="64"/>
        <location filename="../src/operations/extract_pages.cpp" line="79"/>
        <source>Extract…</source>
        <translation>பிரித்தெடு…</translation>
    </message>
    <message>
        <location filename="../src/operations/extract_pages.cpp" line="74"/>
        <location filename="../src/operations/extract_pages.cpp" line="174"/>
        <source>Extract to single PDF</source>
        <translation>ஒற்றை PDF க்கு பிரித்தெடுக்கவும்</translation>
    </message>
    <message>
        <location filename="../src/operations/extract_pages.cpp" line="116"/>
        <source>Select save directory</source>
        <translation>கோப்பகத்தை சேமி என்பதைத் தேர்ந்தெடுக்கவும்</translation>
    </message>
    <message>
        <location filename="../src/operations/extract_pages.cpp" line="178"/>
        <source>PDF files</source>
        <translation>PDF கோப்புகள்</translation>
    </message>
</context>
<context>
    <name>InputPdfFileDelegate</name>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="81"/>
        <source>Page order:</source>
        <translation>பக்க ஒழுங்கு:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="84"/>
        <source>reverse</source>
        <translation>தலைகீழ்</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="86"/>
        <source>forward</source>
        <translation>முன்னோக்கி</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="109"/>
        <source>All</source>
        <translation>அனைத்தும்</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="122"/>
        <source>Pages:</source>
        <translation>பக்கங்கள்:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="125"/>
        <source>Multipage:</source>
        <translation>மல்டிபேச்:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="130"/>
        <source>Disabled</source>
        <translation>முடக்கப்பட்டது</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="133"/>
        <source>Rotation:</source>
        <translation>சுழற்சி:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffiledelegate.cpp" line="136"/>
        <source>Outline entry:</source>
        <translation>அவுட்லைன் நுழைவு:</translation>
    </message>
</context>
<context>
    <name>InputPdfFileWidget</name>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="60"/>
        <source>Reverse page order:</source>
        <translation>பக்க ஒழுங்கு தலைகீழ்:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="74"/>
        <source>Disabled</source>
        <translation>முடக்கப்பட்டது</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="82"/>
        <source>New custom profile…</source>
        <translation>புதிய தனிப்பயன் சுயவிவரம்…</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="84"/>
        <source>No rotation</source>
        <translation>சுழற்சி இல்லை</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="89"/>
        <source>Pages:</source>
        <translation>பக்கங்கள்:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="91"/>
        <source>Multipage:</source>
        <translation>மல்டிபேச்:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="93"/>
        <source>Rotation:</source>
        <translation>சுழற்சி:</translation>
    </message>
    <message>
        <location filename="../src/inputpdffilewidget.cpp" line="95"/>
        <source>Outline entry:</source>
        <translation>அவுட்லைன் நுழைவு:</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="88"/>
        <source>Menu</source>
        <translation>பட்டியல்</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="93"/>
        <source>Multipage profiles…</source>
        <translation>மல்டிபேச் சுயவிவரங்கள்…</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="98"/>
        <source>About</source>
        <translation>பற்றி</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="103"/>
        <source>Exit</source>
        <translation>வெளியேறு</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="378"/>
        <source>Error generating the PDF</source>
        <translation>PDF ஐ உருவாக்கும் பிழை</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="178"/>
        <source>Open PDF file…</source>
        <translation>PDF கோப்பு திறந்த…</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="292"/>
        <source>PDF files</source>
        <translation>PDF கோப்புகள்</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="307"/>
        <source>Error opening file</source>
        <translation>கோப்பைத் திறப்பதில் பிழை</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="332"/>
        <source>Output pages: %1</source>
        <translation>வெளியீட்டு பக்கங்கள்: %1</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="290"/>
        <source>Select a PDF file</source>
        <translation>PDF கோப்பைத் தேர்ந்தெடுக்கவும்</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="362"/>
        <source>Files saved in %1.</source>
        <translation>கோப்புகள் %1 இல் சேமிக்கப்பட்டன.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="368"/>
        <source>File %1 saved.</source>
        <translation>கோப்பு %1 சேமிக்கப்பட்டது.</translation>
    </message>
</context>
<context>
    <name>Merge</name>
    <message>
        <location filename="../src/operations/merge.cpp" line="42"/>
        <source>Merge PDF files</source>
        <translation>PDF கோப்புகளை ஒன்றிணைக்கவும்</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="70"/>
        <source>Edit</source>
        <translation>தொகு</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="71"/>
        <source>View</source>
        <translation>பார்வை</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="74"/>
        <source>Main toolbar</source>
        <translation>முதன்மையான கருவிப்பட்டி</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="81"/>
        <source>Add PDF file</source>
        <translation>PDF கோப்பைச் சேர்க்கவும்</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="86"/>
        <source>Select one or more PDF files to open</source>
        <translation>திறக்க ஒன்று அல்லது அதற்கு மேற்பட்ட PDF கோப்புகளைத் தேர்ந்தெடுக்கவும்</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="88"/>
        <location filename="../src/operations/merge.cpp" line="589"/>
        <source>PDF files</source>
        <translation>PDF கோப்புகள்</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="93"/>
        <source>Move up</source>
        <translation>மேலே செல்லுங்கள்</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="98"/>
        <source>Move down</source>
        <translation>கீழே செல்லுங்கள்</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="103"/>
        <source>Remove file</source>
        <translation>கோப்பை அகற்று</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="113"/>
        <source>Load files list</source>
        <translation>கோப்புகளை ஏற்றவும்</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="118"/>
        <source>Save files list</source>
        <translation>கோப்புகளை சேமிக்கவும்</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="152"/>
        <location filename="../src/operations/merge.cpp" line="156"/>
        <source>Generate PDF</source>
        <translation>PDF ஐ உருவாக்குங்கள்</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="332"/>
        <source>Select the JSON file containing the files list</source>
        <translation>கோப்புகள் பட்டியலைக் கொண்ட சாதொபொகு கோப்பைத் தேர்ந்தெடுக்கவும்</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="334"/>
        <location filename="../src/operations/merge.cpp" line="352"/>
        <source>JSON files (*.json)</source>
        <translation>சாதொபொகு கோப்புகள் (*.JSON)</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="340"/>
        <source>Error while reading the JSON file!</source>
        <translation>சாதொபொகு கோப்பைப் படிக்கும்போது பிழை!</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="341"/>
        <source>An error occurred while reading the JSON file!</source>
        <translation>சாதொபொகு கோப்பைப் படிக்கும்போது பிழை ஏற்பட்டது!</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="348"/>
        <source>Select a JSON file</source>
        <translation>சாதொபொகு கோப்பைத் தேர்ந்தெடுக்கவும்</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="427"/>
        <source>Error opening file</source>
        <translation>கோப்பைத் திறப்பதில் பிழை</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="561"/>
        <source>&lt;p&gt;Output pages of file &lt;b&gt;%1&lt;/b&gt; are badly formatted. Please make sure you complied with the following rules:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;intervals of pages must be written indicating the first page and the last page separated by a dash (e.g. &quot;1-5&quot;);&lt;/li&gt;&lt;li&gt;single pages and intervals of pages must be separated by spaces, commas or both (e.g. &quot;1, 2, 3, 5-10&quot; or &quot;1 2 3 5-10&quot;);&lt;/li&gt;&lt;li&gt;all pages and intervals of pages must be between 1 and the number of pages of the PDF file;&lt;/li&gt;&lt;li&gt;only numbers, spaces, commas and dashes can be used. All other characters are not allowed.&lt;/li&gt;&lt;/ul&gt;</source>
        <translation>&lt;b&gt; கோப்பின் வெளியீட்டு பக்கங்கள் &lt;b&gt;%1 &lt;/b&gt; மோசமாக வடிவமைக்கப்பட்டுள்ளன. பின்வரும் விதிகளுக்கு நீங்கள் இணங்குகிறீர்கள் என்பதை உறுதிப்படுத்திக் கொள்ளுங்கள்: &lt;/p&gt; &lt;ul&gt; &lt;li&gt; பக்கங்களின் இடைவெளிகள் முதல் பக்கத்தையும் கடைசி பக்கத்தையும் ஒரு கோடு (எ.கா. &quot;1-5&quot;) ஐக் குறிக்கும் எழுதப்பட வேண்டும்; &lt;/li &gt; &lt;li&gt; பக்கங்களின் ஒற்றை பக்கங்கள் மற்றும் இடைவெளிகள் இடைவெளிகள், காற்புள்ளிகள் அல்லது இரண்டாலும் பிரிக்கப்பட வேண்டும் (எ.கா. &quot;1, 2, 3, 5-10&quot; அல்லது &quot;1 2 3 5-10&quot;); &lt;/li&gt; &lt;li&gt; பக்கங்களின் அனைத்து பக்கங்களும் இடைவெளிகளும் 1 மற்றும் PDF கோப்பின் பக்கங்களின் எண்ணிக்கையில் இருக்க வேண்டும்; &lt;/li&gt; &lt;li&gt; எண்கள், இடைவெளிகள், காற்புள்ளிகள் மற்றும் கோடுகள் மட்டுமே பயன்படுத்தப்பட முடியும். மற்ற எல்லா எழுத்துகளும் அனுமதிக்கப்படவில்லை. &lt;/Li&gt; &lt;/ul&gt;</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="578"/>
        <source>PDF generation error</source>
        <translation>PDF தலைமுறை பிழை</translation>
    </message>
    <message>
        <location filename="../src/operations/merge.cpp" line="585"/>
        <source>Save PDF file</source>
        <translation>PDF கோப்பைச் சேமிக்கவும்</translation>
    </message>
</context>
<context>
    <name>MultipageEditor</name>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="34"/>
        <source>Standard size</source>
        <translation>நிலையான அளவு</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="35"/>
        <source>Custom size</source>
        <translation>தனிப்பயன் அளவு</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="44"/>
        <source>Portrait</source>
        <translation>உருவப்படம்</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="45"/>
        <source>Landscape</source>
        <translation>நிலப்பரப்பு</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="65"/>
        <source>Right-to-left</source>
        <translation>வலதுபுறம் இடது</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="67"/>
        <location filename="../src/widgets/multipage_editor.cpp" line="202"/>
        <source>Left</source>
        <translation>இடது</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="68"/>
        <location filename="../src/widgets/multipage_editor.cpp" line="72"/>
        <source>Center</source>
        <translation>நடுவண்</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="69"/>
        <location filename="../src/widgets/multipage_editor.cpp" line="205"/>
        <source>Right</source>
        <translation>வலது</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="71"/>
        <location filename="../src/widgets/multipage_editor.cpp" line="208"/>
        <source>Top</source>
        <translation>மேலே</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="73"/>
        <location filename="../src/widgets/multipage_editor.cpp" line="211"/>
        <source>Bottom</source>
        <translation>கீழே</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="106"/>
        <source>Page size</source>
        <translation>பக்க அளவு</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="125"/>
        <source>Orientation:</source>
        <translation>நோக்குநிலை:</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="132"/>
        <source>Width:</source>
        <translation>அகலம்:</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="135"/>
        <source>Height:</source>
        <translation>உயரம்:</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="162"/>
        <source>Pages layout</source>
        <translation>பக்கங்கள் தளவமைப்பு</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="169"/>
        <source>Rows:</source>
        <translation>வரிசைகள்:</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="172"/>
        <source>Columns:</source>
        <translation>நெடுவரிசைகள்:</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="175"/>
        <source>Spacing:</source>
        <translation>இடைவெளி:</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="181"/>
        <source>Pages alignment</source>
        <translation>பக்கங்கள் சீரமைப்பு</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="188"/>
        <source>Horizontal:</source>
        <translation>கிடைமட்டமாக:</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="191"/>
        <source>Vertical:</source>
        <translation>செங்குத்து:</translation>
    </message>
    <message>
        <location filename="../src/widgets/multipage_editor.cpp" line="195"/>
        <source>Margins</source>
        <translation>விளிம்புகள்</translation>
    </message>
</context>
<context>
    <name>MultipageProfilesManager</name>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="31"/>
        <source>New profile…</source>
        <translation>புதிய சுயவிவரம்…</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="35"/>
        <source>Delete profile</source>
        <translation>சுயவிவரத்தை நீக்கு</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="40"/>
        <source>Manage multipage profiles</source>
        <translation>மல்டிபேச் சுயவிவரங்களை நிர்வகிக்கவும்</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="72"/>
        <source>Edit profile</source>
        <translation>சுயவிவரத்தைத் திருத்து</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="118"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="120"/>
        <source>Custom profile</source>
        <translation>தனிப்பயன் சுயவிவரம்</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="179"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="188"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="202"/>
        <source>Error</source>
        <translation>பிழை</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="180"/>
        <source>Profile name can not be empty.</source>
        <translation>சுயவிவர பெயர் காலியாக இருக்க முடியாது.</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="185"/>
        <source>Disabled</source>
        <translation>முடக்கப்பட்டது</translation>
    </message>
    <message>
        <location filename="../src/multipageprofilesmanager.cpp" line="189"/>
        <location filename="../src/multipageprofilesmanager.cpp" line="203"/>
        <source>Profile name already exists.</source>
        <translation>சுயவிவர பெயர் ஏற்கனவே உள்ளது.</translation>
    </message>
</context>
<context>
    <name>PagesSelector</name>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="46"/>
        <source>Pages:</source>
        <translation>பக்கங்கள்:</translation>
    </message>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="47"/>
        <source>Even pages</source>
        <translation>பக்கங்கள் கூட</translation>
    </message>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="48"/>
        <source>Odd pages</source>
        <translation>ஒற்றைப்படை பக்கங்கள்</translation>
    </message>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="50"/>
        <source>All pages</source>
        <translation>அனைத்து பக்கங்களும்</translation>
    </message>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="125"/>
        <source>&lt;p&gt;Page intervals are badly formatted. Please make sure you complied with the following rules:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;intervals of pages must be written indicating the first page and the last page separated by a dash (e.g. &quot;1-5&quot;);&lt;/li&gt;&lt;li&gt;single pages and intervals of pages must be separated by spaces, commas or both (e.g. &quot;1, 2, 3, 5-10&quot; or &quot;1 2 3 5-10&quot;);&lt;/li&gt;&lt;li&gt;all pages and intervals of pages must be between 1 and the number of pages of the PDF file;&lt;/li&gt;&lt;li&gt;only numbers, spaces, commas and dashes can be used. All other characters are not allowed.&lt;/li&gt;&lt;/ul&gt;</source>
        <translation>&lt;p&gt; பக்க இடைவெளிகள் மோசமாக வடிவமைக்கப்பட்டுள்ளன. பின்வரும் விதிகளுக்கு நீங்கள் இணங்குகிறீர்கள் என்பதை உறுதிப்படுத்திக் கொள்ளுங்கள்: &lt;/p&gt; &lt;ul&gt; &lt;li&gt; பக்கங்களின் இடைவெளிகள் முதல் பக்கத்தையும் கடைசி பக்கத்தையும் ஒரு கோடு (எ.கா. &quot;1-5&quot;) ஐக் குறிக்கும் எழுதப்பட வேண்டும்; &lt;/li &gt; &lt;li&gt; பக்கங்களின் ஒற்றை பக்கங்கள் மற்றும் இடைவெளிகள் இடைவெளிகள், காற்புள்ளிகள் அல்லது இரண்டாலும் பிரிக்கப்பட வேண்டும் (எ.கா. &quot;1, 2, 3, 5-10&quot; அல்லது &quot;1 2 3 5-10&quot;); &lt;/li&gt; &lt;li&gt; பக்கங்களின் அனைத்து பக்கங்களும் இடைவெளிகளும் 1 மற்றும் PDF கோப்பின் பக்கங்களின் எண்ணிக்கையில் இருக்க வேண்டும்; &lt;/li&gt; &lt;li&gt; எண்கள், இடைவெளிகள், காற்புள்ளிகள் மற்றும் கோடுகள் மட்டுமே பயன்படுத்தப்பட முடியும். மற்ற எல்லா எழுத்துகளும் அனுமதிக்கப்படவில்லை. &lt;/Li&gt; &lt;/ul&gt;</translation>
    </message>
    <message>
        <location filename="../src/widgets/pages_selector.cpp" line="141"/>
        <source>Error</source>
        <translation>பிழை</translation>
    </message>
</context>
<context>
    <name>PdfInfoLabel</name>
    <message>
        <location filename="../src/widgets/pdfinfolabel.cpp" line="52"/>
        <source>portrait</source>
        <translation>உருவப்படம்</translation>
    </message>
    <message>
        <location filename="../src/widgets/pdfinfolabel.cpp" line="53"/>
        <source>landscape</source>
        <translation>நிலப்பரப்பு</translation>
    </message>
    <message numerus="yes">
        <location filename="../src/widgets/pdfinfolabel.cpp" line="63"/>
        <source>%n page(s)</source>
        <translation>
            <numerusform>%n பக்கம்</numerusform>
            <numerusform>%n பக்கங்கள்</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>Rotate</name>
    <message>
        <location filename="../src/operations/rotate.cpp" line="32"/>
        <source>Rotate</source>
        <translation>சுழற்றுங்கள்</translation>
    </message>
    <message>
        <location filename="../src/operations/rotate.cpp" line="73"/>
        <source>Save as…</source>
        <translation>சேமிக்கவும்…</translation>
    </message>
</context>
</TS>
