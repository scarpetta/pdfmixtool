/* Copyright (C) 2024 Marco Scarpetta
 *
 * This file is part of PDF Mix Tool.
 *
 * PDF Mix Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PDF Mix Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PDF Mix Tool. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CROP_H
#define CROP_H

#include "abstract_operation.h"
#include "../widgets/double_spin_box.h"
#include "../widgets/pages_selector.h"
#include "../widgets/output_preview.h"

#include <QCheckBox>

class Crop : public AbstractOperation
{
    Q_OBJECT
public:
    explicit Crop(QWidget *parent = nullptr);

    void set_pdf_info(const PdfInfo &pdf_info) override;

    int output_pages_count() override;

private slots:
    void keep_aspect_ratio_changed(bool checked);
    void top_changed(double value);
    void bottom_changed(double value);
    void left_changed(double value);
    void right_changed(double value);
    void width_changed(double value);
    void height_changed(double value);
    void prev_page();
    void next_page();

private:
    void adjust_page_size();

    void set_spinbox_maximum();

    void save(bool save_as);

    double m_w_over_h;

    OutputPreview *m_output_preview;
    PagesSelector *m_pages_selector;

    QCheckBox m_keep_aspect_ratio;
    DoubleSpinBox m_top;
    DoubleSpinBox m_bottom;
    DoubleSpinBox m_left;
    DoubleSpinBox m_right;
    DoubleSpinBox m_width;
    DoubleSpinBox m_height;
};

#endif // CROP_H
