/* Copyright (C) 2024 Marco Scarpetta
 *
 * This file is part of PDF Mix Tool.
 *
 * PDF Mix Tool is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * PDF Mix Tool is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with PDF Mix Tool. If not, see <http://www.gnu.org/licenses/>.
 */

#include "double_spin_box.h"

DoubleSpinBox::DoubleSpinBox(QWidget* parent) : QDoubleSpinBox(parent)
{
    connect(this, &DoubleSpinBox::valueChanged,
            this, &DoubleSpinBox::onValueChanged);
}

void DoubleSpinBox::setValueByCode(double value)
{
    m_changed_by_code = true;
    setValue(value);
}

void DoubleSpinBox::onValueChanged(double value)
{
    if(!m_changed_by_code)
        emit valueChangedByUser(value);
    m_changed_by_code = false;
}
